<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Passport\ClientRepository;
use Tests\TestCase;

class CardsTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCardsUnauthenticated()
    {
        $clientRepository = new ClientRepository();
        $client = $clientRepository->createPersonalAccessClient(
            null, 'Test Personal Access Client', 'bills'
        );

        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => $client->id,
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        ]);

        $user = User::create([
            'login' => 'test',
            'password' => bcrypt('123'),
            'api_token' => '12345',
        ]);

        $token = $user->createToken(Str::random(60))->accessToken;
        $headers = ['Authorization' => "Bearer $token", 'Accept' => 'application/json'];

        $this->json('GET', '/api/cards', [], $headers)->assertStatus(401);
    }
}
