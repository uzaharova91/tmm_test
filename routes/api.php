<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\TmmController@userAuth')->name('login');
Route::post('logout', 'Api\TmmController@logout')->name('logout');

Route::group(['middleware' => ['auth:api'], 'namespace' => 'Api'], function () {
    Route::get('bills', 'TmmController@bills')->name('bills');
    Route::get('cards', 'TmmController@cards')->name('cards');
    Route::get('cardDetail', 'TmmController@cardDetail')->name('cardDetail');
});
