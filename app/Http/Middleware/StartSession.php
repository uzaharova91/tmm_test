<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Session\Middleware\StartSession as BaseStartSession;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class StartSession extends BaseStartSession
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user() && ($request->path() != 'api/login')) {
            return response()->json(['error'=>'Unauthorised'], 401);
        }

        if (!Cookie::has('laravel_session') && !empty(Auth::user())) {
            $user = Auth::user();
            User::where('id', $user->id)->delete();
            if ($request->path() !== 'api/login') {
                return response()->json(['message' => 'Unauthenticated.']);
            }
        }

        return parent::handle($request, $next);
    }
}