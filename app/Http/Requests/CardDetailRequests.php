<?php

namespace App\Http\Requests;

class CardDetailRequests extends Request
{
    public const VALIDATION_RULES = [
        'id_card' => 'required|numeric',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::VALIDATION_RULES;
    }
}
