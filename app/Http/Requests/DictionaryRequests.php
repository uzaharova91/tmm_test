<?php

namespace App\Http\Requests;

class DictionaryRequests extends Request
{
    public const VALIDATION_RULES = [
        'name' => 'required|string',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::VALIDATION_RULES;
    }
}
