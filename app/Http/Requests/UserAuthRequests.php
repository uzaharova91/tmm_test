<?php

namespace App\Http\Requests;

class UserAuthRequests extends Request
{
    public const VALIDATION_RULES = [
        'login'    => 'required|string',
        'password' => 'required|string',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::VALIDATION_RULES;
    }
}
