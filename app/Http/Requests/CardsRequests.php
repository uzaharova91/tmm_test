<?php

namespace App\Http\Requests;

class CardsRequests extends Request
{
    public const VALIDATION_RULES = [
        'id_bill' => 'required|string',
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::VALIDATION_RULES;
    }
}
