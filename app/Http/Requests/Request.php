<?php

namespace App\Http\Requests;

use App\Exceptions\ValidationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *  Add sanitize input after validating.
     *
     *  @return void
     */
    public function validate()
    {
        parent::validate();
        $this->sanitize();
    }

    /**
     *  Sanitize this request's input
     *
     *  @return void
     */
    public function sanitize()
    {
        $this->sanitizer = \Sanitizer::make($this->input(), $this->filters());
        $this->replace($this->sanitizer->sanitize());
    }

    /**
     *  Filters to be applied to the input.
     *
     *  @return array
     */
    public function filters()
    {
        return [];
    }

    /**
     * @inheritdoc
     * Overwrite to handle with our validation exception
     */
    protected function failedValidation(Validator $validator)
    {
        throw new \RuntimeException((new ValidationException($validator))->getMessage());
    }
}
