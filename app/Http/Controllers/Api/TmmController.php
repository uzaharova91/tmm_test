<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CardDetailRequests;
use App\Http\Requests\CardsRequests;
use App\Http\Requests\UserAuthRequests;
use App\Service\CardsService;
use App\Service\TmmService;
use App\Service\TokenService;
use App\Service\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Auth;

class TmmController extends Controller
{
    protected const TOKEN_KEY = 'token';

    /** @var TmmService */
    protected $tmmService;

    /** @var CardsService */
    protected $cardsService;

    /** @var TokenService */
    protected $tokenService;

    /** @var UserService */
    protected $userService;

    /**
     * TmmController constructor.
     * @param Request $request
     * @param Container $container
     */
    public function __construct(Request $request, Container $container)
    {
        $this->tmmService = $container->make(TmmService::class);
        $this->cardsService = $container->make(CardsService::class);
        $this->tokenService = $container->make(TokenService::class);
        $this->userService = $container->make(UserService::class);
    }

    public function logout(Request $request)
    {
        $this->userService->deleteUser();
    }

    /**
     * @param UserAuthRequests $request
     * @return JsonResponse
     */
    public function userAuth(UserAuthRequests $request) : JsonResponse
    {
        $token = $this->tokenService->getToken();
        $login = (string) $request->get('login');
        $password = (string) $request->get('password');
        $this->userService->addUser($token, $login, $password);

        if(Auth::attempt(['login' => $request->get('login'), 'password' => $request->get('password')])) {
            $success['token'] = Auth::user()->createToken($token)->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    /**
     * @return JsonResponse
     */
    public function bills(Request $request) : JsonResponse
    {
        $user = Auth::user();
        $result = $this->tmmService->userBills($user->login);
        return response()->json(['result' => $result], 200);
    }

    /**
     * @param CardsRequests $request
     * @return JsonResponse
     */
    public function cards(CardsRequests $request) : JsonResponse
    {
        $cards = $this->tmmService->userCards($request->get('id_bill'));
        if (!empty($cards)) {
            $cards = $this->cardsService->getCardDetail($request->all(), $cards);
        }
        return response()->json(['cards' => $cards], 200);
    }

    /**
     * @param CardDetailRequests $requests
     * @return JsonResponse
     */
    public function cardDetail(CardDetailRequests $requests) : JsonResponse
    {
        $result = $this->tmmService->cardDetail($requests->get('id_card'));
        return response()->json(['card' => $result], 200);
    }
}
