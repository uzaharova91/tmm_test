<?php

namespace App\Service;

/**
 * CardService
 */
class CardsService
{
    const DEFAULT_PAGE = 1;
    const DEFAULT_LIMIT = 10;

    /**
     * @param array $params
     * @param array $cards
     * @return array
     */
    public function getCardDetail(array $params, array $cards)
    {
        if (!empty($params['find_id'])) {
            $id = $params['find_id'];
            $cards = array_filter($cards, function ($card) use ($id) {
                return $card['id'] === $id;
            });
        }

        if (!empty($params['find_description'])) {
            $description = $params['find_description'];
            $cards = array_filter($cards, function ($card) use ($description) {
                return $card['id'] === $description;
            });
        }

        if (!empty($params['filters'])) {
            foreach ($params['filters'] as $key => $filter) {
                $cards = array_filter($cards, function ($card) use ($key, $filter) {
                    return $card[$key] === $filter;
                });
            }
        }

        $page = $params['page'] ?? self::DEFAULT_PAGE;
        $limit = $params['limit'] ?? self::DEFAULT_LIMIT;

        $partCards = array_slice($cards, ($page -1) * $limit, $limit);

        return $partCards;
    }
}
