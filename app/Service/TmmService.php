<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;

/**
 * API
 */
class TmmService
{
    protected const METHOD_POST = 'post';
    protected const METHOD_GET = 'get';

    /**
     * @var Client
     */
    protected $client;

    /**
     * Billing API constructor
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $data
     */
    public function userAuth(array $data)
    {
        $url = $this->getUrl('/Auth');

        $resultData = (bool) $this->sendRequest(self::METHOD_POST, $url, $data);

        return $resultData;
    }

    /**
     * @param string $login
     */
    public function userBills(string $login)
    {
        $url = $this->getUrl('/Bills') . '?login=' . $login;

        $resultData = $this->sendRequest(self::METHOD_GET, $url);

        return empty($resultData['Bill']) ? [] : $resultData['Bill'];
    }

    /**
     * @param string $billId
     * @return array
     */
    public function userCards(string $billId)
    {
        $url = $this->getUrl('/Cards') . '?id_bill=' . $billId;

        $resultData = $this->sendRequest(self::METHOD_GET, $url);

        return empty($resultData['Card']) ? [] : json_decode(json_encode($resultData['Card']), true);
    }

    /**
     * @param int $cardId
     * @return array
     */
    public function cardDetail(int $cardId)
    {
        $url = $this->getUrl('/CardDetail') . '?id_card=' . $cardId;

        try {
            $resultData = $this->sendRequest(self::METHOD_GET, $url);
            Cache::put($this->getCardKey($cardId), $resultData);
        } catch (\Exception $e) {
            $resultData = Cache::get($this->getCardKey($cardId)) ?? [];
        }

        return $resultData;
    }

    /**
     * @param array $data
     */
    public function dictionary(array $data)
    {
        $url = $this->getUrl('/Dictionary') . '?name=' . $data['name'];

        $resultData = $this->sendRequest(self::METHOD_GET, $url);

        return $resultData;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @return array
     */
    protected function sendRequest(string $method, string $url, array $data = []) : array
    {
        return $this->sendRequestAll($method, $url, $data) ?? [];
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $data
     * @return array
     */
    protected function sendRequestAll(string $method, string $url, array $data = []) : array
    {
        try {
            $response = $this->client->request(
                $method,
                $url,
                [
                    'headers' => ['Accept' => 'application/xml'],
                    'timeout' => 120,
                    'form_params' => $data
                ]
            );

            $responseXml = simplexml_load_string($response->getBody()->getContents());

            if (!$responseXml instanceof \SimpleXMLElement) {
                throw new \Exception('Invalid format of response.');
            }

            if (empty($responseXml->Response)) {
                throw new \Exception('Invalid data.');
            }

            return (array) $responseXml->Response;
        } catch (GuzzleClientException $exception) {
            throw new ClientException($exception);
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @param string $path
     * @return string
     */
    private function getUrl(string $path = '/')
    {
        return config('services.tmm.host') . $path;
    }

    /**
     * @param string $id
     * @return string
     */
    private function getCardKey(string $key)
    {
        return 'card_result_' . $key;
    }
}
