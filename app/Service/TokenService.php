<?php

namespace App\Service;

use Illuminate\Support\Str;

/**
 * TokenService
 */
class TokenService
{
    /**
     * @param array $data
     */
    public function getToken()
    {
        return Str::random(32);
    }
}
