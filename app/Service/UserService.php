<?php

namespace App\Service;

use App\User;

/**
 * UserService
 */
class UserService
{
    /**
     * @param string $token
     * @param string $login
     * @param string $password
     */
    public function addUser(string $token, string $login, string $password)
    {
        User::where('login', $login)->delete();

        $data = [
            'login' => $login,
            'password' => bcrypt($password),
            'api_token' => $token,
        ];
        User::create($data);
    }
}
