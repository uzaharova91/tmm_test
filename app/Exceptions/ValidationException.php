<?php

namespace App\Exceptions;

/**
 * Class ValidationException
 *
 * @package App\Exceptions
 */
class ValidationException extends \Illuminate\Validation\ValidationException
{
    /**
     * @inheritdoc
     */
    public function __construct($validator, $response = null, $errorBag = 'default')
    {
        parent::__construct($validator, $response, $errorBag);

        $this->message = 'invalid request data';
    }
}
