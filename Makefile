ENVFILE=.env
MAKEFILE_PATH=.

ifneq ("$(wildcard .env)","")
	# ENVFILE=.env
	APP_DIR=$(shell echo $$(cd . && pwd))
else
	# ENVFILE=../.env
	APP_DIR=$(shell echo $$(cd .. && pwd))
	MAKEFILE_PATH=$(shell basename $$(cd . && pwd))
endif


SHELL = /bin/sh
.DEFAULT_GOAL = help
.PHONY: help update up up_int up_prod down queue app.up app.front app.distclear qa.run-up docker.clear .app.update_env .app.dc.update_env .app.dc.clear

DC=
COPY=cp     # Команда копирования
RM=rm -rf   # Команда удаления
ifeq ($(origin SHLVL), undefined)
	OS_TYPE=Windows
	COPY=copy
else
	ifneq ("$(shell echo $$(uname -s))", "Darwin")
		ifeq ($(shell expr substr $$(uname -s) 1 5), MINGW)
	        	DC=winpty
		endif
	endif
endif


help:  ## Справка по командам
	@echo Usage:
	@echo "   make <command> [<command>, [<command>, ...]]"
	@echo -----
	@echo Available commands:
	@echo "   up             Up projects"
	@echo "   down           Down projects"
	@echo "   update         Update code from microservices and build docker new images"
	@echo "   queue          Run worker queue"
	@echo "   qa.run-up      Merge local develop branch and up"
	@echo "   docker.clear   Clear docker system folder"
	@echo "   up_int         Up projects using dump ./dump/full-int.sql"
	@echo "   up_prod        Up projects using dump ./dump/full-prod.sql"
	@echo -----
	@echo Settings:
	@echo "   APP_DIR:              $(APP_DIR)"
	@echo -----
	@echo Example:
	@echo "   make up          "
	@echo "   make down up     "
	@echo "   make update up   "
	@echo -----

update:
	cd $(APP_DIR) && docker-compose pull

down:
	cd $(APP_DIR) && docker-compose down -v --remove-orphans

up:
	#cd $(APP_DIR) && $(DC) docker-compose rm -f -s -v database redis || echo "clear"
	cd $(APP_DIR) && docker-compose up -d
	cd $(APP_DIR) && $(DC) docker-compose exec app waiting-for-postgresql.sh database
	cd $(APP_DIR) && $(DC) docker-compose exec app composer install
	cd $(APP_DIR) && $(DC) docker-compose exec app php artisan migrate
	cd $(APP_DIR) && $(DC) docker-compose exec app php artisan passport:client --personal
	cd $(APP_DIR) && $(DC) docker-compose exec app php artisan key:generate
	@echo ---------------------------------------------
	@echo =============================================
	@echo == Done
	@echo =============================================

docker.pull:
	cd $(APP_DIR) && docker-compose pull

ifndef CONFIG_PATH
override CONFIG_PATH = backstop.js
endif
