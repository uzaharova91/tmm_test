#!/bin/sh

# Ждем конекта, пока запустится myminio сервер
until /usr/bin/mc config host add myminio "http://0.0.0.0:9000" "${MINIO_ACCESS_KEY}" "${MINIO_SECRET_KEY}" 2>/dev/null; do
  >&2 echo "[MINIO] is unavailable - sleeping"
  sleep 0.3
done


# Пересоздаем bucket
# [WARNING] Хардконо удаляем и создаем новый
# !!! Только для локальных развертываний

/usr/bin/mc rm -r --force myminio/public 2>/dev/null
/usr/bin/mc mb myminio/public
/usr/bin/mc policy public myminio/public

echo "[MINIO] Create: policy public myminio/public"


/usr/bin/mc rm -r --force myminio/private 2>/dev/null
/usr/bin/mc mb myminio/private
/usr/bin/mc policy upload myminio/private

echo "[MINIO] Create: policy upload myminio/private"

exit 0
