#!/usr/bin/env bash
docker-compose stop app
docker pull laravel
docker image prune -f
docker-compose up -d --build app
docker-compose exec app php artisan key:generate
