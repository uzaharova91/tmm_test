Для разворачивания проекта запустите команды:

``docker network create apitmmnetwork``

``make up``

Документация API

Авторизация пользователя, токен используется для получения данных:
/api/auth?login=user1&password=123123

Список счетов:
/api/bills

Список карт по счету:
/api/cards?id_bill=<id>

Детальная информация по карте:
/api/cardDetail?id_card=<id>

При получении данных использовать заголовки (Headers):
Authorization: Bearer <token>
Accept: application/json
